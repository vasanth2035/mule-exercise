**Mule food2fork application to search recipes and to retrieve based on recipe id**

## Application function

This application implements below resources which access food2fork application for retrieving data.

1. search
2. retrieve

## Getting started

## Cloning and running project

Pre-requisites:

1. Install anypoint studio to system
2. Install git-plugin to studio or use any other tools to import project.
3. Check system for java 8 and maven installations.

Running the project:

1. import project into workspace.
2. configure maven into studio(would be required later)

feature development:

1. create a feature branch out of master or sprint-release project.
2. use feature branch for the development
3. commit and push chnages, raise a pull request for merging the code to master or sprint-release branch.

Deployment and version management:

1. code should go deployment only from sprint-release.
2. version needs to incremented in sprint branch -> from snapshot to release after dev testing cloudhub dev environment.
3. currently deployment is manual and is in cloudhub.

Authors:

K U Vasanth Raj.